# PRES -- Protein REad Simulator

Simulate DNA reads from a protein database.

## Input:

* FASTA file with amino acid sequences
* .fai index for the FASTA file (create with `samtools faidx proteins.faa`)
* Output path
* nr of reads
* read length

## Output:

* FASTQ file with DNA reads
* M8 file expected from an analysis

The M8 file mimics a BLAST M8 file, but omits all of the
fields except for the names, identitiy, and length.



## Example:

```bash
$ cargo run -- proteins.faa testreads 5 100
Simulating 5 reads of length 100(nt) from the proteins in file proteins.faa
Writing output to:
testreads.m8
and
testreads.fastq.

$ ls -l
-rw-rw-r-- 1 timm timm 21242 Mar 27 11:22 Cargo.lock
-rw-rw-r-- 1 timm timm   210 Mar 27 11:25 Cargo.toml
-rw-rw-r-- 1 timm timm   408 Mar 27 11:30 README.md
-rw-rw-r-- 1 timm timm  2196 Mar 27 11:45 proteins.faa       <-- Input file
-rw-rw-r-- 1 timm timm   175 Mar 27 11:47 proteins.faa.fai   <-- Input file
drwxrwxr-x 2 timm timm  4096 Mar 27 11:47 src
drwxrwxr-x 3 timm timm  4096 Mar 27 11:22 target
-rw-rw-r-- 1 timm timm  1173 Mar 27 11:53 testreads.fq       <-- generated read file
-rw-rw-r-- 1 timm timm   399 Mar 27 11:53 testreads.m8       <-- generated result file
```

## Limitations:
At the moment no reverse complement reads are generated. Only reading frames 1-3 are simulated.
The missing bases to "fill up" a 100bp read are chosen at random and are not related to the genome.
