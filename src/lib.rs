/// Simulate DNA reads from a FASTA file with protein sequences.
///
/// Usage Example:  cargo run uniprot.faa 20 100
/// 
/// This will create 20 reads of length 100 from uniprot.faa with
/// random padding of 0-2 bases to satisfy desired read length and
/// to simulate different reading frames.
///
/// Note that the nt codons to represent the amino acids are chosen at
/// random with no continuity, hence there can be two completely
/// different (in nt space) reads that map to the same protein position.

extern crate bio;
extern crate rand;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate csv;


use std::error::Error;
use std::process;
use std::fs::File;
use std::io::Write;
use std::path::Path;

use bio::io::fasta;
use bio::io::fastq;
use rand::Rng;
use std::str;

pub mod meta_fai_index;
pub mod conversion;

#[derive(Debug)]
enum Event {
    Perfect,
    Substitution,
    Insertion,
    Deletion,
}

#[derive(Debug)]
enum StructutalChangePosition {
    Prefix,
    Infix,
    Suffix,
}


#[derive(Debug)]
struct Read {
    record: fastq::Record,
    target_protein: String,
    identity: f64,
    hit_pos: u32,
}

impl Read {
    //  1. 	 qseqid 	 query (e.g., gene) sequence id  -> read name
    //  2. 	 sseqid 	 subject (e.g., reference genome) sequence id  -> protein nr
    //  3. 	 pident 	 percentage of identical matches  -> J*
    //  4. 	 length 	 alignment length  -> nr of hits for protein
    //  5. 	 mismatch	 number of mismatches  -> dummy
    //  6. 	 gapopen	 number of gap openings  -> dummy
    //  7. 	 qstart		 start of alignment in query  -> dummy
    //  8. 	 qend		 end of alignment in query  -> dummy
    //  9. 	 sstart		 start of alignment in subject  -> dummy
    //  10. 	 send		 end of alignment in subject  -> dummy
    //  11. 	 evalue 	 expect value  -> dummy 
    //  12. 	 bitscore 	 bit score  -> dummy
    // 
    // 01contig00001_1_491_+    gi|495059223|ref|WP_007784057.1|    29.94    157    100    4    9    157    4    158    2e-11    67.4
    fn m8_entry(&self, name: String) -> String {
        format!("{read_name}\t{prot_nr}\t{percent_ident}\t{length}\t{mismatch}\t{gapopen}\t{qstart}\t{qend}\t{sstart}\t{send}\t{evalue}\t{bitscore}\n",
                read_name = name,
                prot_nr = self.target_protein,
                percent_ident = self.identity,
                length = self.hit_pos,
                mismatch = 0,
                gapopen = 0,
                qstart = 0,
                qend = 0,
                sstart = 0,
                send = 0,
                evalue = 0,
                bitscore = 0,
        )
    }
}

struct Protein {
    seq: Vec<u8>,
    name: String,
    pos: usize,
}


const AMINO_ACIDS: [u8; 20] = [
        65, // A Ala
        86, // V Val
        71, // G Gly
        68, // D Asp
        69, // E Glu
        
        84, // T Thr
        73, // I Ile
        77, // M Met
        83, // S Ser
        82, // R Arg
        78, // N Asn
        75, // K Lys
        
        80, // P Pro
        76, // L Leu
        72, // H His
        81, // Q Gln
        70, // F Phe
        67, // C Cys
        87, // W Trp
        89, // Y Tyr
    ];

const NUCLEOTIDES: [u8; 4] = [65, 67, 71, 84];




/// Main loop
pub fn run(config: Config) -> Result<(), Box<Error>> {
    let mut proteins = Vec::new();
    get_proteins(&config, &mut proteins);
    simulate_reads(&config, proteins);
    Ok(())
}





/// Substitute amino acids in a slice
/// TODO: Add option to only choose M10 / M15 invariate aas
/// Remember to only choose aas that fall into a class of > 1 aas in that case.
#[allow(while_true)]
fn substitute(slice: &mut [u8], subst_pos: usize) {
    let present_aa = slice[subst_pos];
    let mut new_aa = &0;
    while true {
        new_aa = rand::thread_rng().choose(&AMINO_ACIDS).unwrap();
        if *new_aa != present_aa {
            new_aa = new_aa;
            break;
        }
    }
    assert!(*new_aa != 0);
    slice[subst_pos] = *new_aa;
}


/// Read proteins from FASTA file, take ownership, and close file.
fn get_proteins(config: &Config, proteins: &mut Vec<Protein>) {

    // build position -> name index from fai file
    let mfi = meta_fai_index::MetaFaiIndex::with_fasta_file(&config.input_fasta_file)
        .expect(&format!("No .fai index available for {}. Run `samtools faidx {}` to create it.", &config.input_fasta_file, &config.input_fasta_file));
    let prots_in_file = mfi.seqs.len();

    // initialize rng
    let mut rng = rand::thread_rng();
    
    // randomly choose protein numbers
    let mut prot_indices = Vec::new();
    for _ in 0..config.nr_reads {
        let prot_nr: usize = rng.gen_range(0, prots_in_file);
        prot_indices.push(prot_nr);
    }
    
    // optionally sort the chosen chosen protein numbers to
    // make the output sorted
    if config.grouped {
        prot_indices.sort();
    }
    
    // get protein names from indices
    let mut prot_names = Vec::new();
    for prot_ind in prot_indices.iter() {
        prot_names.push((mfi.inner.get(prot_ind).unwrap(), prot_ind));
    }

    // Read protein sequences and push them into a vector.
    // For big test sets this has to be replaced with an iterator
    let mut reader = fasta::IndexedReader::from_file(&config.input_fasta_file).expect("Failed openening the fasta file");
    for (name, prot_ind) in prot_names {
        let mut protein = Vec::new();
        reader.read_all(name, &mut protein).unwrap();
        // Create owned copies of each protein.
        // This is most certainly inefficient, but will suffice for this task.
        let seq = protein.to_owned();
        proteins.push(Protein{seq, name:name.to_owned(), pos: *prot_ind});
    }
}


/// Create n reads of length m from the k proteins read from file.
fn simulate_reads(config: &Config, proteins: Vec<Protein>) {
    // initialize rng
    let mut rng = rand::thread_rng();
    
    // Assemble lists of all possible enum values to choose from
    let all_events = vec![Event::Perfect, Event::Substitution, Event::Insertion, Event::Deletion];
    let all_positions = vec![StructutalChangePosition::Prefix, StructutalChangePosition::Infix, StructutalChangePosition::Suffix];

    // Assemble output_paths
    let m8_path = &(config.output_path.clone() + ".m8");
    let fastq_path = &(config.output_path.clone() + ".fq");

    // open output files
    let mut m8_file = File::create(m8_path)
        .expect(&format!("Opening m8 file {} did not work. Check that the path exists.", m8_path));
    let mut fq_writer = fastq::Writer::to_file(fastq_path)
        .expect(&format!("Opening fastq file {} did not work. Check that the path exists.", fastq_path));
    

    // Main simulation loop
    for (i, active_prot) in proteins.iter().enumerate() {
        if i > 0 && (i % 1000 == 0) {
            eprint!("\rFinished {}/{} random reads.", i, proteins.len());
        }
        
        // Pick an event (perfect, substitution etc.)
        let event = rand::thread_rng().choose(&all_events).unwrap();

        // compute length of the 
        let prot_len: usize = active_prot.seq.len() - 1;
        let aa_read_length = config.read_length / 3;

        if prot_len <= aa_read_length {
            eprintln!{"Protein is too short to generate read. Skipped."}
            continue
        }

        // define variables that will be filled differently for each event type
        let indel_len;
        let startpos;
        let stoppos;
        let slice;
        let identity;
        let mut quality_values = vec![b'I'; aa_read_length * 3];  // One QV for every nt without padding

        // handle events
        match event {
            &Event::Insertion => {
                // reduce number of aas from the protein as the insertion is filled with random aas

                // determine how long the insert will be, where it is located
                indel_len = rand::thread_rng().gen_range::<usize>(1, 10);
                let position = rand::thread_rng().choose(&all_positions).unwrap();

                // Get the postiitons of the aas that are taken from the protein
                startpos = rng.gen_range(0, prot_len - aa_read_length);
                stoppos = startpos + aa_read_length - indel_len;
                let slice_length = stoppos - startpos;

                // populate the random insert.
                // TODO this can be moved into a function
                let mut random_insert = Vec::new();
                for _ in 0..indel_len {
                    random_insert.push(rand::thread_rng().choose(&AMINO_ACIDS).unwrap().clone());
                }
               
                // Create a slice according to the determined position
                // modify the quality values according to the insert
                match position {
                    &StructutalChangePosition::Prefix => {
                        slice = [&random_insert[..], &active_prot.seq[startpos..stoppos]].concat();
                        for aa_pos in 0 .. (stoppos - startpos){
                            for nt_offset in 0..3 {
                                quality_values[3 * aa_pos + nt_offset] = b'#';
                            }
                        }
                    },
                    &StructutalChangePosition::Infix => {
                        let slice_insert_point = rand::thread_rng().gen_range::<usize>(1, slice_length - 1);
                        let insert_point = startpos + slice_insert_point;
                        slice = [&active_prot.seq[startpos..insert_point], &random_insert[..], &active_prot.seq[insert_point..stoppos]].concat();
                        for aa_pos in slice_insert_point .. (slice_insert_point + indel_len){
                            for nt_offset in 0..3 {
                                quality_values[3 * aa_pos + nt_offset] = b'#';
                            }
                        }
                    },
                    &StructutalChangePosition::Suffix => {
                        slice = [&active_prot.seq[startpos..stoppos], &random_insert[..]].concat();
                        for aa_pos in (stoppos - startpos) .. aa_read_length {
                            for nt_offset in 0..3 {
                                quality_values[3 * aa_pos + nt_offset] = b'#';
                            }
                        }
                    },
                }
                identity = 1.0 - ((indel_len  as f64 * 3.0) / config.read_length as f64);
            },
            
            &Event::Deletion => {
                // increase number of read aas as a part will be dropped

                // Determine how long the deletion will be and where it is located (front, middle, end).
                let max_del_length = prot_len - aa_read_length;
                if max_del_length < 1 {
                    eprintln!("Read too short to support a deletion. Skipped.");
                    continue;
                } else if max_del_length > 10 {
                    // make sure deletions are maximally 10 bases long
                    indel_len = rand::thread_rng().gen_range::<usize>(1, 10);
                    startpos = rng.gen_range(0, prot_len - (aa_read_length + indel_len));
                } else if max_del_length == 1 {
                    // only delete one aa since only this fits the protein size
                    indel_len = 1;
                    startpos = 0;
                } else {
                    // randomly pick a maximum deletion length (guaranteed to be between 2 and 10 inclusively)
                    indel_len = rand::thread_rng().gen_range::<usize>(1, max_del_length);
                    startpos = rng.gen_range(0, prot_len - (aa_read_length + indel_len));
                }
                let position = rand::thread_rng().choose(&all_positions).unwrap();

                // Get the positions of aas that are taken from the original protein.
                // eprintln!("prot_len: {}   aa_read_len: {}    indel_len: {}", prot_len, aa_read_length, indel_len);
                stoppos = startpos + aa_read_length + indel_len;
                let slice_length = stoppos - startpos;

                // Create a slice that satisfies the deletion.
                // The quality values are not influenced by this.
                match position {
                    &StructutalChangePosition::Prefix => {
                        slice = active_prot.seq[(startpos + indel_len)..stoppos].to_vec();
                    },
                    &StructutalChangePosition::Infix => {
                        let deletion_point = startpos + rand::thread_rng().gen_range::<usize>(1, slice_length - (indel_len + 1));
                        slice = [&active_prot.seq[startpos..deletion_point], &active_prot.seq[(deletion_point + indel_len)..stoppos]].concat();
                    },
                    &StructutalChangePosition::Suffix => {
                        slice = active_prot.seq[startpos..(stoppos - indel_len)].to_vec();
                    },
                }
                identity = 1.0 - (8.0 / config.read_length as f64); //  8 = kmer length -> fraction of kmers that hit the deletion
            },
            
            &Event::Substitution => {
                // Determine how many substitutions the will be.
                let substitution_number = rand::thread_rng().gen_range::<u32>(1, 10);
                // Get the positions of aas that are taken from the original protein.
                startpos = rng.gen_range(0, prot_len - aa_read_length);
                stoppos = startpos + aa_read_length;
                let slice_length = stoppos - startpos;
                // Get the (unmodified) slice from the protein
                let mut extracted_slice = active_prot.seq[startpos..stoppos].to_vec();

                for _ in 0..substitution_number {
                    // Determine where this substitution will be located.
                    // TODO make sure double replacements do not happen.
                    let subst_pos = rand::thread_rng().gen_range::<usize>(0, slice_length);
                    substitute(&mut extracted_slice, subst_pos);
                    for nt_offset in 0..3 {
                        quality_values[3 * subst_pos + nt_offset] = b'.';
                    }
                }
                slice = extracted_slice;
                identity = 1.0 - (substitution_number as f64 / slice_length as f64);
            },

            _ => {
                // Perfect read. Take a slice and use it as is.
                startpos = rng.gen_range(0, prot_len - aa_read_length);
                stoppos = startpos + aa_read_length;
                slice = active_prot.seq[startpos..stoppos].to_vec();
                identity = 1.0;
            },
        }
        
        // Convert aa slice to nucleotide read (that might be too short)
        let mut nt_read = conversion::aa_to_nt(&slice[..]);

        // Pad the read with random bases until the desired length is satisfied.
        // Also pad the quality values withg the value '*' to denote padding.
        // TODO To test all reading frames: randomize for each base:
        //   - append to front or back
        //   - revcomp or not
        for _i in 0 .. (config.read_length - nt_read.len()){
            let padding_base = rng.choose(&NUCLEOTIDES).unwrap();
            nt_read.insert(0, padding_base.clone());
            quality_values.insert(0, b'*');
        }

        let name = format!("Read_{:_>10}_from_pos_{:_>5}_in_{:_<30}", i, startpos, active_prot.name);
        // Assemble a read object for writing.
        let r = Read{
            record: fastq::Record::with_attrs(
                &name,
                None,
                &nt_read, // str::from_utf8(&nt_read).unwrap(),
                &quality_values, // String::from_utf8(vec![b'#'; nt_read.len()]).unwrap()
            ),
            target_protein: active_prot.name.clone(),
            identity: identity,
            hit_pos: 42,  // KLUDGE: Dummy value
        };

        // Write to output files.
        m8_file.write(r.m8_entry(name).as_bytes()).expect("Failed to write to m8 file!");
        fq_writer.write_record(&r.record).expect("Failed to write to fastq file!");
    }

    eprintln!("Writing random reads.");
    // Write random reads here.
    let random_reads_nr = (config.nr_reads as f64 * 0.1) as usize;
    for i in 0 .. random_reads_nr {
        if i > 0 && (i % 1000 == 0) {
            eprintln!("Finished {}/{} random reads.", i, random_reads_nr);
        }
        let aa_read_length = config.read_length / 3;
        
        let mut slice = Vec::new();
        let mut quality_values = vec![b'#'; aa_read_length * 3];  // One QV for every nt without padding
        for _ in 0..aa_read_length {
            slice.push(*rand::thread_rng().choose(&AMINO_ACIDS).unwrap());
        }

        // Convert aa slice to nucleotide read (that might be too short)
        let mut nt_read = conversion::aa_to_nt(&slice[..]);

        // Pad the read with random bases until the desired length is satisfied.
        // Also pad the quality values withg the value '*' to denote padding.
        // TODO To test all reading frames: randomize for each base:
        //   - append to front or back
        //   - revcomp or not
        // eprintln!("config read length: {}, nt read length: {}", config.read_length, nt_read.len());
        
        for _i in 0 .. (config.read_length - nt_read.len()){
            let padding_base = rng.choose(&NUCLEOTIDES).unwrap();
            nt_read.insert(0, padding_base.clone());
            quality_values.insert(0, b'*');
        }

        let name = format!("Random_Read_{:.>10}", i);
        // Assemble a read object for writing.
        let r = Read{
            record: fastq::Record::with_attrs(
                &name,
                None,
                &nt_read, // str::from_utf8(&nt_read).unwrap(),
                &quality_values, // String::from_utf8(vec![b'#'; nt_read.len()]).unwrap()
            ),
            target_protein: String::from("NONE. Random Read"),
            identity: 0.0,
            hit_pos: 42,  // KLUDGE: Dummy value
        };

        // Write to output files.
        m8_file.write(r.m8_entry(name).as_bytes()).expect("Failed to write to m8 file!");
        fq_writer.write_record(&r.record).expect("Failed to write to fastq file!");
    }
}




/// Class to handle user parameters
/// The read length is given in nt.
pub struct Config {
    pub input_fasta_file: String,
    pub output_path: String,
    pub nr_reads: usize,
    pub read_length: usize,
    pub grouped: bool,
}


impl Config {

    /// Parse command line args.
    pub fn new(args: &[String]) -> Result<Config, &'static str> {

        if args.len() != 5 {
            return Err("Usage:\n\n  $ cargo run <fasta file> <output path> <nr of reads> <read length in nt>\n\nThe output path should be with filename, without suffix. Example: /tmp/testreads");
        }
        
        let input_fasta_file = args[1].clone();
        let raw_output_path = Path::new(&args[2]);
        let output_path;
        match raw_output_path.extension() {
            Some(ext) => {
                let stop = &args[2].len() - (ext.len() + 1);
                output_path = String::from(&args[2][..stop]);
            },
            None      => {
                output_path = args[2].clone();
            },
        }
        let nr_reads: usize = args[3].parse().unwrap_or_else(|err| {
            eprintln!("Could not parse nr_reads string `{}` to u64, because: {}", args[3], err);
            process::exit(1);
        });
        let read_length: usize = args[4].parse().unwrap_or_else(|err| {
            eprintln!("Could not parse read_length string `{}` to u64, because: {}", args[4], err);
            process::exit(1);
        });
        let grouped = true;
        
        Ok(Config{ input_fasta_file, output_path, nr_reads, read_length, grouped })
    }
}
