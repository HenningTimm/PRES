extern crate testdata_simulation;

use std::env;
use std::process;

use testdata_simulation::Config;


fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments. {}", err);
        process::exit(1);
    });

    eprintln!("Simulating {} reads of length {}(nt) from the proteins in file {}",
              config.nr_reads, config.read_length, config.input_fasta_file);

    eprintln!("Writing output to:\n{}\nand\n{}",
              config.output_path.clone() + ".m8", config.output_path.clone() + ".fq");
    
    if let Err(e) = testdata_simulation::run(config) {
        eprintln!("Application Error {}", e);
        process::exit(1);
    }
}
