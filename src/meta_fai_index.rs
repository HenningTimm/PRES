use std::{collections, fs, io};
use std::path::Path;

use csv;



/// Record of a FASTA index.
#[derive(Deserialize, Debug, Copy, Clone)]
struct IndexRecord {
    len: u64,
    offset: u64,
    line_bases: u64,
    line_bytes: u64,
}

/// A FASTA index as created by SAMtools (.fai).
pub struct MetaFaiIndex {
    pub inner: collections::HashMap<usize, String>,
    pub seqs: Vec<String>,
}


// TODO can this be achieved with the seqs list only?
// use index to access seqs list and the name to query the dictionary.

impl MetaFaiIndex {
    /// Open a FASTA index from a given `io::Read` instance.
    pub fn new<R: io::Read>(fai: R) -> csv::Result<Self> {
        let mut inner = collections::HashMap::new();
        let mut seqs = vec![];
        let mut fai_reader = csv::ReaderBuilder::new()
            .delimiter(b'\t')
            .has_headers(false)
            .from_reader(fai);
        for (index, row) in fai_reader.deserialize().enumerate() {
            let (name, _record): (String, IndexRecord) = row?;
            seqs.push(name.clone());
            inner.insert(index, name);
        }
        Ok(MetaFaiIndex {
               inner: inner,
               seqs: seqs,
           })
    }

    /// Open a FASTA index from a given file path.
    pub fn from_file<P: AsRef<Path>>(path: &P) -> csv::Result<Self> {
        fs::File::open(path)
           .map_err(csv::Error::from)
           .and_then(Self::new)
    }

    /// Open a FASTA index given the corresponding FASTA file path.
    /// That is, for ref.fasta we expect ref.fasta.fai.
    pub fn with_fasta_file<P: AsRef<Path>>(fasta_path: &P) -> csv::Result<Self> {
        let mut fai_path = fasta_path.as_ref().as_os_str().to_owned();
        fai_path.push(".fai");

        Self::from_file(&fai_path)
    }
}
