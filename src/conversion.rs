use std::collections::HashMap;
use rand;
use rand::Rng;


/// Convert an aa slice to a nt read.
pub fn aa_to_nt(aa_read: &[u8]) -> Vec<u8> {
    let mut nt_read = Vec::new();
    let conv = Conversion::new();
    for aa in aa_read{
        match conv.translate(aa) {
            Some(nt) => {
                // Can this be optimized
                nt_read.push(nt[0]);
                nt_read.push(nt[1]);
                nt_read.push(nt[2]);
            },
            None => {
                // Push in NNN
                nt_read.push(78);
                nt_read.push(78);
                nt_read.push(78);
            }
        }
    }
    nt_read
}



/// Struct that holds a conversion table from aa to nt
/// and allows translation of amino acids to codons that
/// satisfy the aa. The codon is chosen uniformly at random
/// from all codons that translate to the given aa.
struct Conversion {
    conversion_table: HashMap<u8, Vec<Vec<u8>>>,
}


impl Conversion {
    
    /// create and populate the mapping table and generate a new conversion object.
    pub fn new() -> Conversion {
        let mut conversion_table = HashMap::new();
        conversion_table.insert(65, vec![vec![71, 67, 65], vec![71, 67, 67], vec![71, 67, 71], vec![71, 67, 84]]); // A Ala
        conversion_table.insert(86, vec![vec![71, 84, 65], vec![71, 84, 67], vec![71, 84, 71], vec![71, 84, 84]]); // V Val
        conversion_table.insert(71, vec![vec![71, 71, 65], vec![71, 71, 67], vec![71, 71, 71], vec![71, 71, 84]]); // G Gly
        conversion_table.insert(68, vec![vec![71, 65, 67], vec![71, 65, 84]]); // D Asp
        conversion_table.insert(69, vec![vec![71, 65, 65], vec![71, 65, 71]]); // E Glu
        
        conversion_table.insert(84, vec![vec![65, 67, 65], vec![65, 67, 67], vec![65, 67, 71], vec![65, 67, 84]]); // T Thr
        conversion_table.insert(73, vec![vec![65, 84, 65], vec![65, 84, 67], vec![65, 84, 84]]); // I Ile
        conversion_table.insert(77, vec![vec![65, 84, 71]]); // M Met
        conversion_table.insert(83, vec![vec![65, 71, 67], vec![65, 71, 84], vec![84, 67, 65], vec![84, 67, 67], vec![84, 67, 71], vec![84, 67, 84]]); // S Ser
        conversion_table.insert(82, vec![vec![65, 71, 65], vec![65, 71, 71], vec![67, 71, 65], vec![67, 71, 67], vec![67, 71, 71], vec![67, 71, 84]]); // R Arg
        conversion_table.insert(78, vec![vec![65, 65, 67], vec![65, 65, 84]]); // N Asn
        conversion_table.insert(75, vec![vec![65, 65, 65], vec![65, 65, 71]]); // K Lys
        
        conversion_table.insert(80, vec![vec![67, 67, 65], vec![67, 67, 67], vec![67, 67, 71], vec![67, 67, 84]]); // P Pro
        conversion_table.insert(76, vec![vec![67, 84, 65], vec![67, 84, 67], vec![67, 84, 71], vec![67, 84, 84], vec![84, 84, 65], vec![84, 84, 71]]); // L Leu
        conversion_table.insert(72, vec![vec![67, 65, 67], vec![67, 65, 84]]); // H His
        conversion_table.insert(81, vec![vec![67, 65, 65], vec![67, 65, 71]]); // Q Gln
        conversion_table.insert(70, vec![vec![84, 84, 67], vec![84, 84, 84]]); // F Phe
        conversion_table.insert(67, vec![vec![84, 71, 67], vec![84, 71, 84]]); // C Cys
        conversion_table.insert(87, vec![vec![84, 71, 71]]); // W Trp
        conversion_table.insert(89, vec![vec![84, 65, 67], vec![84, 65, 84]]); // Y Tyr

        conversion_table.insert(85, vec![vec![84, 67, 65]]); // U Sec
        conversion_table.insert(79, vec![vec![84, 65, 71]]); // O Pyl 
        Conversion {conversion_table}
    }

    
    /// Translate an amino acid to a codon.
    /// This codon is chosen uniformly at random from all
    /// codons encoding the given aa.
    pub fn translate(&self, key: &u8) -> Option<&Vec<u8>> {
        
        let candidates;
        match self.conversion_table.get(key) {
            Some(c) => candidates = c,
            None => {
                // eprintln!("Could not find a codon for aa `{}`. using 'NNN' instead.", key);
                return None
            },
        }
        
        match rand::thread_rng().choose(&candidates) {
            Some(codon) => return Some(codon),
            None => {
                // eprintln!("Could not pick a codon from `{:?}`", candidates);
                return None
                },
        }
    }
}
